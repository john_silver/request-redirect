# Heroku Express Generator Boilerplate
This is a small repo to begin a Heroku Express Generator Boilerplate, with ejs as engine view.

## Prerequisites
+ Heroku CLI
+ Node.js
+ express-generator


## Installation & Run

```sh
# Remove .git folder and start again
rm -rf .git

# Start a new repo
git init

# Create Heroku App
heroku create myApp

# Install dependencies
npm install

#Add repo files
git add .

# Commit changes
git commit -m 'Initial commit'

#Push Boilerplate to Heroku
git push heroku master

#Open Heroku App
heroku open

```

