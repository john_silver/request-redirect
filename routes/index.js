var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/redirect', function(req, res, next) {
  console.log('get index page');
  console.log( 'cookie ' + JSON.stringify(req.cookies));
  console.log('res cookie ' + req.cookies['connect.sid']);
  if(!req.cookies['foo']){
    console.log('redirect');
    res.writeHead(302, {'Content-Type': 'text/plain', 'Location': '/redirect', 'Set-Cookie': 'foo=bar'});
    // res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('redirect with cookie...');
    
  }
  else {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('Final Content' + JSON.stringify(req.cookies));

      
  }
});

module.exports = router;
